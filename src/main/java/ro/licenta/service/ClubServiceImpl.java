package ro.licenta.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ro.licenta.exception.RecordNotFoundException;
import ro.licenta.model.Club;
import ro.licenta.repository.ClubRepository;

@Service("clubService")
public class ClubServiceImpl implements ClubService{
	
	private static final Logger logger = LoggerFactory.getLogger(ClubServiceImpl.class);
	
	@Autowired
	private ClubRepository clubRepository;
	
	@Override
	public Club addClub(Club club) {
		// TODO Auto-generated method stub
		clubRepository.save(club);
		return club;
	}

	@Override
	public Club updateClub(Club club) {
		// TODO Auto-generated method stub
		Club existingClub = clubRepository.findById(club.getId()).orElse(null);
		if(existingClub == null) {
			String errorMessage = "Club with id " + club.getId() + " not found.";
			logger.error(errorMessage);
			throw new RecordNotFoundException(errorMessage);
		}
		return clubRepository.save(club);
	}

	@Override
	public void deleteClub(Long clubId) {
		// TODO Auto-generated method stub
		Club club = clubRepository.findById(clubId).orElse(null);
		logger.debug("Deleting the club with id : " + clubId);
		if(club != null) {
			clubRepository.deleteById(clubId);
		} else {
			String errorMessage = "Club with id " + clubId + " not found.";
			logger.error(errorMessage);
			throw new RecordNotFoundException(errorMessage);
		}
	}

	@Override
	public Club findClub(Long clubId) {
		// TODO Auto-generated method stub
		Club club = clubRepository.findById(clubId).orElse(null);
		return club;
	}

	@Override
	public List<Club> getAllClubs() {
		// TODO Auto-generated method stub
		return clubRepository.findAll();
	}

}
