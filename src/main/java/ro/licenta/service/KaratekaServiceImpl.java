package ro.licenta.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ro.licenta.exception.RecordNotFoundException;
import ro.licenta.model.Karateka;
import ro.licenta.repository.KaratekaRepository;

@Service("karatekaService")
public class KaratekaServiceImpl implements KaratekaService{
	
	private static final Logger logger = LoggerFactory.getLogger(KaratekaServiceImpl.class);
	
	@Autowired
	private KaratekaRepository karatekaRepository;
	
	@Override
	public Karateka addKarateka(Karateka karateka) {
		// TODO Auto-generated method stub
		karatekaRepository.save(karateka);
		return karateka;
	}

	@Override
	public Karateka updateKarateka(Karateka karateka) {
		// TODO Auto-generated method stub
		Karateka existingKarateka = karatekaRepository.findById(karateka.getId()).orElse(null);
		if(existingKarateka == null) {
			String errorMessage = "Karateka with id : " + karateka.getId() + " not found.";
			logger.error(errorMessage);
			throw new RecordNotFoundException(errorMessage);
		}
		return karatekaRepository.save(karateka);
	}

	@Override
	public void deleteKarateka(Long karatekaId) {
		// TODO Auto-generated method stub
		Karateka karateka = karatekaRepository.findById(karatekaId).orElse(null);
		if(karateka != null) {
			karatekaRepository.deleteById(karatekaId);
		} else {
			String errorMessage = "Karateka with id : " + karatekaId + " not found.";
			logger.error(errorMessage);
			throw new RecordNotFoundException(errorMessage);
		}
	}

	@Override
	public Karateka findKarateka(Long karatekaId) {
		// TODO Auto-generated method stub
		Karateka karateka = karatekaRepository.findById(karatekaId).orElse(null);
		return karateka;
	}

	@Override
	public List<Karateka> getAllKaratekas() {
		// TODO Auto-generated method stub
		return karatekaRepository.findAll();
	}

	@Override
	public Karateka findKarateka(String lastName) {
		// TODO Auto-generated method stub
		return karatekaRepository.findByLastName(lastName);
	}

}
