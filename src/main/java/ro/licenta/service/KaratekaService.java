package ro.licenta.service;

import java.util.List;

import ro.licenta.model.Karateka;

public interface KaratekaService {
	
	Karateka addKarateka(Karateka karateka);
	
	Karateka updateKarateka(Karateka karateka);
	
	void deleteKarateka(Long karatekaId);
	
	Karateka findKarateka(Long karatekaId);
	
	List<Karateka> getAllKaratekas();
	
	Karateka findKarateka(String lastName);
}
