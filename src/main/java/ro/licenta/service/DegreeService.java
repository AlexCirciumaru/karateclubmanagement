package ro.licenta.service;

import java.util.List;

import ro.licenta.model.Degree;

public interface DegreeService {
	
	Degree addDegree(Degree degree);
	
	Degree updateDegree(Degree degree);
	
	void deleteDegree(Long degreeId);
	
	Degree findDegree(Long degreeId);
	
	List<Degree> getAllDegrees();
}
