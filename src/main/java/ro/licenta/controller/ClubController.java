package ro.licenta.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ro.licenta.dto.ClubDTO;
import ro.licenta.model.Club;
import ro.licenta.service.ClubService;

@Controller
public class ClubController {

	private static Logger logger = LoggerFactory.getLogger(ClubController.class);

	@Autowired
	private ClubService clubService;

	@RequestMapping(value = "/clubs", method = RequestMethod.GET)
	public String getClubs(Model model) {
		List<Club> clubs = clubService.getAllClubs();
		model.addAttribute("clubs", clubs);

		return "clubs";
	}

	@RequestMapping(value = "/club/add", method = RequestMethod.GET)
	public String getAddClubForm(Model model) {
		List<Club> clubs = clubService.getAllClubs();
		model.addAttribute("clubs", clubs);

		ClubDTO clubDTO = new ClubDTO();
		model.addAttribute("club", clubDTO);

		return "add-club";
	}

	@RequestMapping(value = "/club/add", method = RequestMethod.POST)
	public String addClubForm(@Valid @ModelAttribute("club") ClubDTO clubDTO, BindingResult result, Model model,
			RedirectAttributes redirectAtttributes) {
		if (result.hasErrors()) {
			logger.error("Add club error : " + result.getAllErrors());
			List<Club> clubs = clubService.getAllClubs();
			model.addAttribute("clubs", clubs);

			return "add-club";
		} else {
			Club club = new Club();
			club.setClubName(clubDTO.getClubName());
			club.setAddress(clubDTO.getAddress());
			club.setDateOfEstablishment(clubDTO.getDateOfEstablishment());

			clubService.addClub(club);
			redirectAtttributes.addFlashAttribute("message", "Successfully added.");
			return "redirect:/clubs";
		}
	}

	@RequestMapping(value = "/club/update", method = RequestMethod.GET)
	public String getClubEditForm(@RequestParam(value = "id", required = true) Long id, Model model,
			RedirectAttributes redirectAttributes) {
		Club club = clubService.findClub(id);
		logger.debug("Edit club : ", club);
		if (club != null) {
			// Create and put ClubDTO needed to edit the club.
			ClubDTO clubDTO = new ClubDTO();
			clubDTO.setId(club.getId());
			clubDTO.setClubName(club.getClubName());
			clubDTO.setAddress(club.getAddress());
			clubDTO.setDateOfEstablishment(club.getDateOfEstablishment());

			model.addAttribute("club", clubDTO);

/*			// Add to model supplementary attributes needed to construct the edit form.
			// Add list of clubs to model to populate the clubs selector.
			List<Club> clubs = clubService.getAllClubs();
			model.addAttribute("clubs", clubs);*/
			return "update-club";
		} else {
			logger.error("Edit error : Club with id {} not found.", id);
			redirectAttributes.addFlashAttribute("message", "Club with specified id not found.");
			return "redirect:/clubs";
		}
	}

	@RequestMapping(value = "/club/update", method = RequestMethod.POST)
	public String updateClub(@Valid @ModelAttribute("club") ClubDTO clubDTO, BindingResult result,
			Model model, RedirectAttributes redirectAttributes) {
		if(result.hasErrors()) {
			logger.error("Update club validation error : " + result.getAllErrors());
			/*// Add to model supplementary attributes needed to construct the edit form.
			// Add list of clubs to model to populate the clubs selector.
			List<Club> clubs = clubService.getAllClubs();
			model.addAttribute("clubs", clubs);*/
			return "update-club";			
		} else {			
			Club club = new Club();
			club.setId(clubDTO.getId());
			club.setClubName(clubDTO.getClubName());
			club.setAddress(clubDTO.getAddress());
			club.setDateOfEstablishment(clubDTO.getDateOfEstablishment());
			
			clubService.updateClub(club);
			
			redirectAttributes.addFlashAttribute("message", "Club successfully updated.");
			return "redirect:/clubs";
		}
	}
	
	@RequestMapping(value = "/club/delete", method = RequestMethod.GET)
	public String deleteClub(@Valid @ModelAttribute("id") Long id, BindingResult result,
			RedirectAttributes redirectAttributes) {
		try {
			clubService.deleteClub(id);
			redirectAttributes.addFlashAttribute("message", "Successfully deleted.");
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("errorMessage", "Delete error : " + e.getMessage());
		}
		
		return "redirect:/clubs";
	}
	
	@InitBinder
	public void initBinder(final WebDataBinder binder){
	  final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd"); 
	  binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
}
