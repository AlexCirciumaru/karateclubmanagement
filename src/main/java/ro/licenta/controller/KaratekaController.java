package ro.licenta.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ro.licenta.dto.KaratekaDTO;
import ro.licenta.model.Club;
import ro.licenta.model.Karateka;
import ro.licenta.service.ClubService;
import ro.licenta.service.KaratekaService;

@Controller
public class KaratekaController {

	private static Logger logger = LoggerFactory.getLogger(KaratekaController.class);

	@Autowired
	private KaratekaService karatekaService;

	@Autowired
	private ClubService clubService;

	@RequestMapping(value = "/karatekas", method = RequestMethod.GET)
	public String getKaratekas(Model model) {
		List<Karateka> karatekas = karatekaService.getAllKaratekas();
		model.addAttribute("karatekas", karatekas);
		return "karatekas";
	}

	@RequestMapping(value = "/karateka/add", method = RequestMethod.GET)
	public String getAddKaratekaForm(Model model) {
		// Add list of clubs to model to populate the clubs selector.
		List<Club> clubs = clubService.getAllClubs();
		model.addAttribute("clubs", clubs);

		KaratekaDTO karatekaDTO = new KaratekaDTO();
		model.addAttribute("karateka", karatekaDTO);
		return "add-karateka";
	}

	/**
	 * This method will be called on form submission, handling POST request for
	 * saving task in database. It also validates the task input
	 */
	@RequestMapping(value = "/karateka/add", method = RequestMethod.POST)
	public String addKaratekaForm(@Valid @ModelAttribute("karateka") KaratekaDTO karatekaDTO, BindingResult result,
			Model model, RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			logger.error("Add Karateka error : " + result.getAllErrors());
			// Add list of clubs to model to populate the clubs selector.
			List<Club> clubs = clubService.getAllClubs();
			model.addAttribute("clubs", clubs);

			return "add-karateka";
		} else {
			Karateka karateka = new Karateka();
			karateka.setLastName(karatekaDTO.getLastName());
			karateka.setFirstName(karatekaDTO.getFirstName());
			karateka.setAge(karatekaDTO.getAge());
			karateka.setBeginningYear(karatekaDTO.getBeginningYear());
			karateka.setEmail(karatekaDTO.getEmail());
			karateka.setPassword(karatekaDTO.getPassword());
			karateka.setTrainer(karatekaDTO.isTrainer());
			karateka.setClub(new Club(karatekaDTO.getClubId()));
			
			karatekaService.addKarateka(karateka);
			redirectAttributes.addFlashAttribute("message", "Successfully added.");
			return "redirect:/karatekas";
		}
	}

	@RequestMapping(value = "/karateka/update", method = RequestMethod.GET)
	public String getKaratekaEditForm(@RequestParam(value = "id", required = true) Long id, Model model,
			RedirectAttributes redirectAttributes) {
		Karateka karateka = karatekaService.findKarateka(id);
		logger.debug("Edit karateka : ", karateka);
		if (karateka != null) {
			// Create and put a KaratekaDTO needed to edit karateka.
			KaratekaDTO karatekaDTO = new KaratekaDTO();
			karatekaDTO.setId(karateka.getId());
			karatekaDTO.setLastName(karateka.getLastName());
			karatekaDTO.setFirstName(karateka.getFirstName());
			karatekaDTO.setAge(karateka.getAge());
			karatekaDTO.setBeginningYear(karateka.getBeginningYear());
			karatekaDTO.setEmail(karateka.getEmail());
			karatekaDTO.setPassword(karateka.getPassword());
			karatekaDTO.setTrainer(karateka.isTrainer());
			karatekaDTO.setClubId(karateka.getClub().getId());

			model.addAttribute("karateka", karatekaDTO);

			// Add to model supplementary attributes needed to construct the edit form.
			// Add list of karatekas to model to populate the karatekas selector.
			List<Club> clubs = clubService.getAllClubs();
			model.addAttribute("clubs", clubs);
			return "update-karateka";
		} else {
			logger.error("Edit error: Karateka with id {} not found.", id);
			redirectAttributes.addFlashAttribute("errorMessage", "Karateka with specified id not found.");
			return "redirect:/karatekas";
		}
	}

	@RequestMapping(value = "/karateka/update", method = RequestMethod.POST)
	public String updateKarateka(@Valid @ModelAttribute("karateka") KaratekaDTO karatekaDTO, BindingResult result,
			Model model, RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			logger.error("Update karateka validation error : " + result.getAllErrors());
			// Add to model supplementary attributes needed to construct the edit form.
			// Add list of karatekas to model to populate the karatekas selector.
			List<Club> clubs = clubService.getAllClubs();
			model.addAttribute("clubs", clubs);
			return "update-karateka";
		} else {
			Karateka karateka = new Karateka();
			karateka.setId(karatekaDTO.getId());
			karateka.setFirstName(karatekaDTO.getFirstName());
			karateka.setLastName(karatekaDTO.getLastName());
			karateka.setAge(karatekaDTO.getAge());
			karateka.setBeginningYear(karatekaDTO.getBeginningYear());
			karateka.setEmail(karatekaDTO.getEmail());
			karateka.setPassword(karatekaDTO.getPassword());
			karateka.setTrainer(karatekaDTO.isTrainer());
			karateka.setClub(new Club(karatekaDTO.getClubId()));

			karatekaService.updateKarateka(karateka);

			redirectAttributes.addFlashAttribute("message", "Karateka updated successfully.");
			return "redirect:/karatekas";
		}
	}

	@RequestMapping(value = "/karateka/delete", method = RequestMethod.GET)
	public String deleteKarateka(@Valid @ModelAttribute("id") Long id, BindingResult result,
			RedirectAttributes redirectAttributes) {
		try {
			karatekaService.deleteKarateka(id);
			redirectAttributes.addFlashAttribute("message", "Successfully deleted.");
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("errorMessage", "Delete error : " + e.getMessage());
		}

		return "redirect:/karatekas";
	}
}
