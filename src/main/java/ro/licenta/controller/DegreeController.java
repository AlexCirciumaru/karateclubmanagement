package ro.licenta.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ro.licenta.dto.DegreeDTO;
import ro.licenta.model.Degree;
import ro.licenta.service.DegreeService;

@Controller
public class DegreeController {
	
	private static Logger logger = LoggerFactory.getLogger(DegreeController.class);
	
	@Autowired
	private DegreeService degreeService;
	
	@RequestMapping(value = "/degrees", method = RequestMethod.GET)
	public String getDegrees(Model model) {
		List<Degree> degrees = degreeService.getAllDegrees();
		model.addAttribute("degrees", degrees);			
		return "degrees";
	}
	
	@RequestMapping(value = "/degree/add", method = RequestMethod.GET)
	public String getAddDegreeForm(Model model) {
		List<Degree> degrees = degreeService.getAllDegrees();
		model.addAttribute("degrees", degrees);
		
		DegreeDTO degreeDTO = new DegreeDTO();
		model.addAttribute("degree", degreeDTO);
		return "add-degree";
	}
	
	@RequestMapping(value = "/degree/add", method = RequestMethod.POST)
	public String addDegreeForm(@Valid @ModelAttribute("degree") DegreeDTO degreeDTO, BindingResult result,
			Model model, RedirectAttributes redirectAttributes) {
		if(result.hasErrors()) {
			logger.error("Add degree error : " + result.getAllErrors());
			/*List<Degree> degrees = degreeService.getAllDegrees();
			model.addAttribute("degrees", degrees);*/
			return "add-degree";
		} else {
			Degree degree = new Degree();
			degree.setKyuRank(degreeDTO.getKyuRank());
			degree.setBeltColor(degreeDTO.getBeltColor());
			
			degreeService.addDegree(degree);
			redirectAttributes.addFlashAttribute("message", "Successfully added.");			
			return "redirect:/degrees";
		}
	}
	
	@RequestMapping(value = "/degree/update", method = RequestMethod.GET)
	public String getDegreeEditForm(@RequestParam(value = "id", required = true) Long id, Model model,
			RedirectAttributes redirectAttributes) {
		Degree degree = degreeService.findDegree(id);
		logger.debug("Edit degree : " + degree);
		if(degree != null) {
			//Create and put DegreeDTO needed to edit degree.
			DegreeDTO degreeDTO = new DegreeDTO();
			degreeDTO.setId(degree.getId());
			degreeDTO.setKyuRank(degree.getKyuRank());
			degreeDTO.setBeltColor(degree.getBeltColor());
			
			model.addAttribute("degree", degreeDTO);
			
			/*// Add to model supplementary attributes needed to construct the edit form.
			// Add list of degrees to model to populate the degrees selector.
			
			List<Degree> degrees = degreeService.getAllDegrees();
			model.addAttribute("degrees", degrees);*/
			return "update-degree";
		} else {
			logger.error("Edit error : Degree with id {} not found." + id);
			redirectAttributes.addFlashAttribute("errorMessage", "Degree with specified it not found.");
			return "redirect:/degrees";
		}
	}
	
	@RequestMapping(value = "/degree/update", method = RequestMethod.POST)
	public String updateDegree(@Valid @ModelAttribute("degree") DegreeDTO degreeDTO, BindingResult result,
			Model model, RedirectAttributes redirectAttributes) {
		if(result.hasErrors()) {
			logger.error("Update degree validation error." + result.getAllErrors());
/*			// Add to model supplementary attributes needed to construct the edit form.
			// Add list of degrees to model to populate the degrees selector.
			List<Degree> degrees = degreeService.getAllDegrees();
			model.addAttribute("degrees", degrees);*/
			return "update-degree";
		} else {
			Degree degree = new Degree();
			degree.setId(degreeDTO.getId());
			degree.setKyuRank(degreeDTO.getKyuRank());
			degree.setBeltColor(degreeDTO.getBeltColor());
			
			degreeService.updateDegree(degree);
			
			redirectAttributes.addFlashAttribute("message", "Degree updated successfully.");
			return "redirect:/degrees";
		}
	}
	
	@RequestMapping(value = "/degree/delete", method = RequestMethod.GET)
	public String deleteDegree(@Valid @ModelAttribute("id") Long id, BindingResult result,
			RedirectAttributes redirectAttributes) {
		try {
			degreeService.deleteDegree(id);
			redirectAttributes.addFlashAttribute("message", "Degree deleted successfully.");
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("errorMessage", "Delete error : " + e.getMessage());
		}
		
		return "redirect:/degrees";
	}
}
