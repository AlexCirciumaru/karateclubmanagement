package ro.licenta.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ro.licenta.dto.KaratekaDegreeDTO;
import ro.licenta.model.Club;
import ro.licenta.model.Degree;
import ro.licenta.model.Karateka;
import ro.licenta.model.KaratekaDegree;
import ro.licenta.service.ClubService;
import ro.licenta.service.DegreeService;
import ro.licenta.service.KaratekaDegreeService;
import ro.licenta.service.KaratekaService;

@Controller
public class KaratekaDegreeController {

	private static Logger logger = LoggerFactory.getLogger(KaratekaDegreeController.class);

	@Autowired
	private KaratekaDegreeService karatekaDegreeService;

	@Autowired
	private KaratekaService karatekaService;

	@Autowired
	private DegreeService degreeService;

	@Autowired
	private ClubService clubService;

	@RequestMapping(value = "/karatekaDegrees", method = RequestMethod.GET)
	public String getKaratekaDegrees(Model model) {
		List<KaratekaDegree> karatekaDegrees = karatekaDegreeService.getAllKaratekaDegrees();
		model.addAttribute("karatekaDegrees", karatekaDegrees);

		return "karateka-degrees";
	}

	@RequestMapping(value = "/karatekaDegree/add", method = RequestMethod.GET)
	public String getAddKaratekaDegreeForm(Model model) {

		// Add to model supplementary attributes needed to construct the add form.

		// Add list of karatekas to model to populate the karatekas selector.
		List<Karateka> karatekas = karatekaService.getAllKaratekas();
		model.addAttribute("karatekas", karatekas);

		// Add list of degrees to populate the degrees selector.
		List<Degree> degrees = degreeService.getAllDegrees();
		model.addAttribute("degrees", degrees);

		// Add list of clubs to populate the clubs selector.
		List<Club> clubs = clubService.getAllClubs();
		model.addAttribute("clubs", clubs);

		// Add list to populate the trainer selector.
		List<Karateka> temporaryList = karatekaService.getAllKaratekas();
		List<Karateka> trainers = new ArrayList<Karateka>();
		for (Karateka karateka : temporaryList) {
			if (karateka.isTrainer()) {
				trainers.add(karateka);
			}
		}
		model.addAttribute("trainers", trainers);

		KaratekaDegreeDTO karatekaDegreeDTO = new KaratekaDegreeDTO();
		model.addAttribute("karatekaDegree", karatekaDegreeDTO);

		return "add-karateka-degree";
	}

	@RequestMapping(value = "/karatekaDegree/add", method = RequestMethod.POST)
	public String addKaratekaDegreeForm(@Valid @ModelAttribute("karatekaDegree") KaratekaDegreeDTO karatekaDegreeDTO,
			BindingResult result, Model model, RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			logger.error("Add Karateka Degree error : " + result.getAllErrors());

			// Add to model supplementary attributes needed to construct the add form.

			// Add list of karatekas to model to populate the karatekas selector.
			List<KaratekaDegree> karatekaDegrees = karatekaDegreeService.getAllKaratekaDegrees();
			model.addAttribute("karatekaDegrees", karatekaDegrees);

			// Add list of degrees to populate the degrees selector.
			List<Degree> degrees = degreeService.getAllDegrees();
			model.addAttribute("degrees", degrees);

			// Add list of clubs to populate the clubs selector.
			List<Club> clubs = clubService.getAllClubs();
			model.addAttribute("clubs", clubs);

			// Add list to populate the trainer selector.
			List<Karateka> temporaryList = karatekaService.getAllKaratekas();
			List<Karateka> trainers = new ArrayList<Karateka>();
			for (Karateka karateka : temporaryList) {
				if (karateka.isTrainer()) {
					trainers.add(karateka);
				}
			}
			model.addAttribute("trainers", trainers);

			return "add-karateka-degree";
		} else {
			KaratekaDegree karatekaDegree = new KaratekaDegree();
			karatekaDegree.setKarateka(new Karateka(karatekaDegreeDTO.getKaratekaId()));
			karatekaDegree.setDegree(new Degree(karatekaDegreeDTO.getDegreeId()));
			karatekaDegree.setDateOfReceipt(karatekaDegreeDTO.getDateOfReceipt());
			karatekaDegree.setClub(new Club(karatekaDegreeDTO.getClubId()));
			karatekaDegree.setTrainer(new Karateka(karatekaDegreeDTO.getTrainerId()));

			karatekaDegreeService.addKaratekaDegree(karatekaDegree);
			redirectAttributes.addFlashAttribute("message", "Successfully added.");
			return "redirect:/karatekaDegrees";
		}
	}

	@RequestMapping(value = "/karatekaDegree/update", method = RequestMethod.GET)
	public String getKaratekaDegreeEditForm(@RequestParam(value = "id", required = true) Long id, Model model,
			RedirectAttributes redirectAttributes) {
		KaratekaDegree karatekaDegree = karatekaDegreeService.findKaratekaDegree(id);
		logger.debug("Edit Karateka Degree : ", karatekaDegree);
		if (karatekaDegree != null) {
			// Create and put KaratekaDegreeDTO to edit the KaratekaDegree.
			KaratekaDegreeDTO karatekaDegreeDTO = new KaratekaDegreeDTO();
			karatekaDegreeDTO.setId(karatekaDegree.getId());
			karatekaDegreeDTO.setKaratekaId(karatekaDegree.getKarateka().getId());
			karatekaDegreeDTO.setDegreeId(karatekaDegree.getDegree().getId());
			karatekaDegreeDTO.setDateOfReceipt(karatekaDegree.getDateOfReceipt());
			karatekaDegreeDTO.setClubId(karatekaDegree.getClub().getId());
			karatekaDegreeDTO.setTrainerId(karatekaDegree.getTrainer().getId());

			model.addAttribute("karatekaDegree", karatekaDegreeDTO);
			
			// Add to model supplementary attributes needed to construct the add form.

			// Add list of karatekas to model to populate the karatekas selector.
			List<Karateka> karatekas = karatekaService.getAllKaratekas();
			model.addAttribute("karatekas", karatekas);

			// Add list of degrees to populate the degrees selector.
			List<Degree> degrees = degreeService.getAllDegrees();
			model.addAttribute("degrees", degrees);

			// Add list of clubs to populate the clubs selector.
			List<Club> clubs = clubService.getAllClubs();
			model.addAttribute("clubs", clubs);

			// Add list to populate the trainer selector.
			List<Karateka> temporaryList = karatekaService.getAllKaratekas();
			List<Karateka> trainers = new ArrayList<Karateka>();
			for (Karateka karateka : temporaryList) {
				if (karateka.isTrainer()) {
					trainers.add(karateka);
				}
			}
			model.addAttribute("trainers", trainers);


			return "update-karateka-degree";
		} else {
			logger.error("Edit error : Karateka Degree with id {} not found.", id);
			redirectAttributes.addFlashAttribute("message", "Karateka Degree with specified id not found.");
			return "redirect:/karatekaDegrees";
		}
	}

	@RequestMapping(value = "/karatekaDegree/update", method = RequestMethod.POST)
	public String updateKaratekaDegree(@Valid @ModelAttribute("karatekaDegree") KaratekaDegreeDTO karatekaDegreeDTO,
			BindingResult result, Model model, RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			logger.error("Update Karateka Degree validation error : " + result.getAllErrors());
			
			// Add to model supplementary attributes needed to construct the add form.

			// Add list of karatekas to model to populate the karatekas selector.
			List<Karateka> karatekas = karatekaService.getAllKaratekas();
			model.addAttribute("karatekas", karatekas);

			// Add list of degrees to populate the degrees selector.
			List<Degree> degrees = degreeService.getAllDegrees();
			model.addAttribute("degrees", degrees);

			// Add list of clubs to populate the clubs selector.
			List<Club> clubs = clubService.getAllClubs();
			model.addAttribute("clubs", clubs);

			// Add list to populate the trainer selector.
			List<Karateka> temporaryList = karatekaService.getAllKaratekas();
			List<Karateka> trainers = new ArrayList<Karateka>();
			for (Karateka karateka : temporaryList) {
				if (karateka.isTrainer()) {
					trainers.add(karateka);
				}
			}
			model.addAttribute("trainers", trainers);

			return "update-karateka-degree";
		} else {
			KaratekaDegree karatekaDegree = new KaratekaDegree();
			karatekaDegree.setId(karatekaDegreeDTO.getId());
			karatekaDegree.setKarateka(new Karateka(karatekaDegreeDTO.getKaratekaId()));
			karatekaDegree.setDegree(new Degree(karatekaDegreeDTO.getDegreeId()));
			karatekaDegree.setDateOfReceipt(karatekaDegreeDTO.getDateOfReceipt());
			karatekaDegree.setClub(new Club(karatekaDegreeDTO.getClubId()));
			karatekaDegree.setTrainer(new Karateka(karatekaDegreeDTO.getTrainerId()));

			karatekaDegreeService.updateKaratekaDegree(karatekaDegree);

			redirectAttributes.addFlashAttribute("message", "Karateka Degree successfully updated.");
			return "redirect:/karatekaDegrees";
		}
	}

	@RequestMapping(value = "/karatekaDegree/delete", method = RequestMethod.GET)
	public String deleteKarateDegree(@Valid @ModelAttribute("id") Long id, BindingResult result,
			RedirectAttributes redirectAttributes) {
		try {
			karatekaDegreeService.deleteKaratekaDegree(id);
			redirectAttributes.addFlashAttribute("message", "Successfully deleted.");
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("errorMessage", "Delete error : " + e.getMessage());
		}

		return "redirect:/karatekaDegrees";
	}

	@InitBinder
	public void initBinder(final WebDataBinder binder) {
		final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
}
