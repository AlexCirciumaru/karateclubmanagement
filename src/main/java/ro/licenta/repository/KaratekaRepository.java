package ro.licenta.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ro.licenta.model.Karateka;

@Repository("karatekaRepository")
public interface KaratekaRepository extends JpaRepository<Karateka, Long>, JpaSpecificationExecutor<Karateka>{
	
	@Query("SELECT p FROM Karateka p WHERE p.lastName = :lastName")
	Karateka findByLastName(@Param("lastName") String lastName);
	
	Karateka findByFirstName(@Param("firstName") String firstName);
	
	@Query("SELECT p FROM Karateka p WHERE p.lastName LIKE :searchTerm OR p.firstName LIKE :searchTerm")
	public List<Karateka> search(@Param("searchTerm") String searchTerm);
}
