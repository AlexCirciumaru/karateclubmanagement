package ro.licenta.dto;

import javax.validation.constraints.NotBlank;

public class DegreeDTO {

	private Long id;
	
	@NotBlank(message = "Kyu rank cannot be empty.")
	private String kyuRank;
	
	@NotBlank(message = "Belt Color cannot be empty.")
	private String beltColor;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKyuRank() {
		return kyuRank;
	}

	public void setKyuRank(String kyuRank) {
		this.kyuRank = kyuRank;
	}

	public String getBeltColor() {
		return beltColor;
	}

	public void setBeltColor(String beltColor) {
		this.beltColor = beltColor;
	}
}
