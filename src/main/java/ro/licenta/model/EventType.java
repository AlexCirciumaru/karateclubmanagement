package ro.licenta.model;

public enum EventType {
	DEMONSTRATION, SEMINAR, COMPETITION;
}
