package ro.licenta.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "degree")
public class Degree {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "kyu_rank", nullable = false)
	private String kyuRank;
	
	@Column(name = "belt_color", nullable = false)
	private String beltColor;
	
	public Degree() {		
	}
	
	public Degree(Long id) {
		super();
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKyuRank() {
		return kyuRank;
	}

	public void setKyuRank(String kyuRank) {
		this.kyuRank = kyuRank;
	}

	public String getBeltColor() {
		return beltColor;
	}

	public void setBeltColor(String beltColor) {
		this.beltColor = beltColor;
	}
}
