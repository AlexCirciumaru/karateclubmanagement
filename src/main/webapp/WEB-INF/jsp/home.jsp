<%@ include file="header.jsp"%>
<!-- Page content (Begin)  -->

<div class="container myrow-container">
	<div class="panel panel-success">
		<div class="panel-heading">
			<div align="left">
				<h3 class="panel-title">
					<b>Home</b>
				</h3>
			</div>
			<div align="right">
				<h3 class="panel-title">
					<a href="<c:url value='/karatekas'/>">View Karatekas List</a> | <a
						href="<c:url value='/clubs'/>">View Clubs List</a> | <a
						href="<c:url value='/degrees'/>">View Degrees List</a> | <a
						href="<c:url value='/karatekaDegrees'/>">View History</a>
				</h3>
			</div>
		</div>
	</div>
</div>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<!-- Page content (End)    -->
<%@ include file="footer.jsp"%>