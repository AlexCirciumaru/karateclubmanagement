<%@ include file="header.jsp" %>
<!-- Page content (Begin)  -->

<%@ include file="home.jsp" %>

<div class="container myrow-container">
		<div class="panel panel-success">
			<div class="panel-heading">
				<div align="left">
					<h3 class="panel-title">
						<b>Clubs List</b>
					</h3>
				</div>
				<div align="right">
					<h3 class="panel-title">
						<a href="<c:url value='/club/add'/>">Add New Club</a>
					</h3>
				</div>
			</div>
			<div class="panel-body">
				<c:if test="${empty clubs}">
                There are no clubs
                <form action="searchclub">
						<div class="row">
							<div class="col-md-6">
								<div class="col-md-6">Search clubs:</div>
								<div class="col-md-6">
									<input type="text" name="clubName" id="clubName">
								</div>
							</div>
							<div class="col-md-4">
								<input class="btn btn-success" type='submit' value='Search' />
							</div>
						</div>
					</form>
				</c:if>
				<c:if test="${not empty clubs}">

					<form action="searchclub">
						<div class="row">
							<div class="col-md-6">
								<div class="col-md-6">Search clubs:</div>
								<div class="col-md-6">
									<input type="text" name="clubName" id="clubName">
								</div>
							</div>
							<div class="col-md-4">
								<input class="btn btn-success" type='submit' value='Search' />
							</div>
						</div>
					</form>

					<table class="table table-hover table-bordered">
						<thead style="background-color: #bce8f1;">
							<tr>
								<th>Id</th>
								<th>Club Name</th>
								<th>Address</th>
								<th>Date of Establishment</th>
								<th>Edit</th>
								<th>Delete</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${clubs}" var="club">
								<tr>
									<th><c:out value="${club.id}" /></th>
									<th><c:out value="${club.clubName}" /></th>
									<th><c:out value="${club.address}" /></th>
									<th><c:out value="${club.dateOfEstablishment}" /></th>
									<th><a href="<c:url value='/club/update?id=${club.id}'/>">Edit</a></th>
									<th><a href="<c:url value='/club/delete?id=${club.id}'/>">Delete</a></th>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</c:if>
			</div>
		</div>
	</div>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<!-- Page content (End)    -->
<%@ include file="footer.jsp" %>