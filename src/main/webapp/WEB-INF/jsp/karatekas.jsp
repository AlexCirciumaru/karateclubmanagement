<%@ include file="header.jsp" %>
<!-- Page content (Begin)  -->

<%@ include file="home.jsp" %>

<div class="container myrow-container">
		<div class="panel panel-success">
			<div class="panel-heading">
				<div align="left">
					<h3 class="panel-title">
						<b>Karatekas List</b>
					</h3>
				</div>
				<div align="right">
					<h3 class="panel-title">
						<a href="<c:url value='/karateka/add'/>">Add New Karateka</a>
					</h3>
				</div>
			</div>
			<div class="panel-body">
				<c:if test="${empty karatekas}">
                There are no Karatekas
                <form action="search">
						<div class="row">
							<div class="col-md-6">
								<div class="col-md-6">Search Karatekas:</div>
								<div class="col-md-6">
									<input type="text" name="lastName" id="lastName">
								</div>
							</div>
							<div class="col-md-4">
								<input class="btn btn-success" type='submit' value='Search' />
							</div>
						</div>
					</form>
				</c:if>
				<c:if test="${not empty karatekas}">

					<form action="search">
						<div class="row">
							<div class="col-md-6">
								<div class="col-md-6">Search Karatekas:</div>
								<div class="col-md-6">
									<input type="text" name="lastName" id="lastName">
								</div>
							</div>
							<div class="col-md-4">
								<input class="btn btn-success" type='submit' value='Search' />
							</div>
						</div>
					</form>

					<table class="table table-hover table-bordered">
						<thead style="background-color: #bce8f1;">
							<tr>
								<th>Id</th>
								<th>First Name</th>
								<th>Last Name</th>
								<th>Age</th>
								<th>Beginning Year</th>
								<th>Club</th>
								<th>Email</th>
								<th>Edit</th>
								<th>Delete</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${karatekas}" var="karateka">
								<tr>
									<th><c:out value="${karateka.id}" /></th>
									<th><c:out value="${karateka.firstName}" /></th>
									<th><c:out value="${karateka.lastName}" /></th>
									<th><c:out value="${karateka.age}" /></th>
									<th><c:out value="${karateka.beginningYear}" /></th>
									<th><c:out value="${karateka.club.clubName}" /></th>
									<th><c:out value="${karateka.email}" /></th>
									<th><a href="<c:url value='/karateka/update?id=${karateka.id}'/>">Edit</a></th>
									<th><a href="<c:url value='/karateka/delete?id=${karateka.id}'/>">Delete</a></th>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</c:if>
			</div>
		</div>
	</div>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<!-- Page content (End)    -->
<%@ include file="footer.jsp" %>