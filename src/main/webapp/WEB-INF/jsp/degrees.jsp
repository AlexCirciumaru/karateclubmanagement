<%@ include file="header.jsp" %>
<!-- Page content (Begin)  -->

<%@ include file="home.jsp" %>

<div class="container myrow-container">
		<div class="panel panel-success">
			<div class="panel-heading">
				<div align="left">
					<h3 class="panel-title">
						<b>Degrees List</b>
					</h3>
				</div>
				<div align="right">
					<h3 class="panel-title">
						<a href="<c:url value='/degree/add'/>">Add New Degree</a>
					</h3>
				</div>
			</div>
			<div class="panel-body">
				<c:if test="${empty degrees}">
                There are no degrees
                <form action="searchDegree">
						<div class="row">
							<div class="col-md-6">
								<div class="col-md-6">Search Degrees :</div>
								<div class="col-md-6">
									<input type="text" name="beltColor" id="beltColor">
								</div>
							</div>
							<div class="col-md-4">
								<input class="btn btn-success" type='submit' value='Search' />
							</div>
						</div>
					</form>
				</c:if>
				<c:if test="${not empty degrees}">

					<form action="searchDegree">
						<div class="row">
							<div class="col-md-6">
								<div class="col-md-6">Search Degrees:</div>
								<div class="col-md-6">
									<input type="text" name="beltColor" id="beltColor">
								</div>
							</div>
							<div class="col-md-4">
								<input class="btn btn-success" type='submit' value='Search' />
							</div>
						</div>
					</form>

					<table class="table table-hover table-bordered">
						<thead style="background-color: #bce8f1;">
							<tr>
								<th>Id</th>
								<th>Belt Color</th>
								<th>KyuRank</th>
								<th>Edit</th>
								<th>Delete</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${degrees}" var="degree">
								<tr>
									<th><c:out value="${degree.id}" /></th>
									<th><c:out value="${degree.beltColor}" /></th>
									<th><c:out value="${degree.kyuRank}" /></th>
									<th><a href="<c:url value='/degree/update?id=${degree.id}'/>">Edit</a></th>
									<th><a href="<c:url value='/degree/delete?id=${degree.id}'/>">Delete</a></th>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</c:if>
			</div>
		</div>
	</div>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<!-- Page content (End)    -->
<%@ include file="footer.jsp" %>