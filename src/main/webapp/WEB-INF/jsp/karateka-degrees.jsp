<%@ include file="header.jsp" %>
<!-- Page content (Begin)  -->

<%@ include file="home.jsp" %>

<div class="container myrow-container">
		<div class="panel panel-success">
			<div class="panel-heading">
				<div align="left">
					<h3 class="panel-title">
						<b>Karateka Degrees List</b>
					</h3>
				</div>
				<div align="right">
					<h3 class="panel-title">
						<a href="<c:url value='/karatekaDegree/add'/>">Add New Karateka Degree</a>
					</h3>
				</div>
			</div>
			<div class="panel-body">
				<c:if test="${empty karatekaDegrees}">
                There are no Karateka Degrees
                <form action="searchKaratekaDegree">
						<div class="row">
							<div class="col-md-6">
								<div class="col-md-6">Search Karateka Degrees:</div>
								<div class="col-md-6">
									<input type="text" name="karateka.lastName" id="karateka.lastName">
								</div>
							</div>
							<div class="col-md-4">
								<input class="btn btn-success" type='submit' value='Search' />
							</div>
						</div>
					</form>
				</c:if>
				<c:if test="${not empty karatekaDegrees}">

					<form action="searchKaratekaDegree">
						<div class="row">
							<div class="col-md-6">
								<div class="col-md-6">Search Karateka Degrees:</div>
								<div class="col-md-6">
									<input type="text" name="karateka.lastName" id="karateka.lastName">
								</div>
							</div>
							<div class="col-md-4">
								<input class="btn btn-success" type='submit' value='Search' />
							</div>
						</div>
					</form>

					<table class="table table-hover table-bordered">
						<thead style="background-color: #bce8f1;">
							<tr>
								<th>Id</th>
								<th>Karateka Name</th>
								<th>Degree</th>
								<th>Date of Receipt</th>
								<th>Club</th>
								<th>Trainer</th>
								<th>Edit</th>
								<th>Delete</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${karatekaDegrees}" var="karatekaDegree">
								<tr>
									<th><c:out value="${karatekaDegree.id}" /></th>
									<th><c:out value="${karatekaDegree.karateka.lastName}" /></th>
									<th><c:out value="${karatekaDegree.degree.beltColor}" /></th>
									<th><c:out value="${karatekaDegree.dateOfReceipt}" /></th>
									<th><c:out value="${karatekaDegree.club.clubName}" /></th>
									<th><c:out value="${karatekaDegree.trainer.lastName}" /></th>
									<th><a href="<c:url value='/karatekaDegree/update?id=${karatekaDegree.id}'/>">Edit</a></th>
									<th><a href="<c:url value='/karatekaDegree/delete?id=${karatekaDegree.id}'/>">Delete</a></th>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</c:if>
			</div>
		</div>
	</div>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<!-- Page content (End)    -->
<%@ include file="footer.jsp" %>