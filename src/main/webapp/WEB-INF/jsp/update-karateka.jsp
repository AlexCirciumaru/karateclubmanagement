<%@ include file="header.jsp" %>
<!-- Page content (Begin)  -->

<div class="container myrow-container">
	<div class="panel panel-success">
		<div class="panel-heading">
			<h1>Update Karateka</h1>
		</div>
		<div class="panel-body">
			<form:form cssClass="form-horizontal" method="post"
				modelAttribute="karateka">

				<div class="form-group">
					<div class="control-label col-xs-3">
						<form:label path="lastName">Last Name</form:label>
					</div>
					<div class="col-xs-6">
						<form:hidden path="id" value="${karatekaObject.id}" />
						<form:input cssClass="form-control" path="lastName"
							value="${karatekaObject.lastName}" />
					</div>
				</div>
				
				<div class="form-group">
					<div class="control-label col-xs-3">
						<form:label path="firstName">First Name</form:label>
					</div>
					<div class="col-xs-6">
						<form:hidden path="id" value="${karatekaObject.id}" />
						<form:input cssClass="form-control" path="firstName"
							value="${karatekaObject.firstName}" />
					</div>
				</div>
				
				<div class="form-group">
					<div class="control-label col-xs-3">
						<form:label path="age">Age</form:label>
					</div>
					<div class="col-xs-6">
						<form:hidden path="id" value="${karatekaObject.id}" />
						<form:input cssClass="form-control" path="age"
							value="${karatekaObject.age}" />
					</div>
				</div>
				
				<div class="form-group">
					<div class="control-label col-xs-3">
						<form:label path="beginningYear">Beginning Year</form:label>
					</div>
					<div class="col-xs-6">
						<form:hidden path="id" value="${karatekaObject.id}" />
						<form:input cssClass="form-control" path="beginningYear"
							value="${karatekaObject.beginningYear}" />
					</div>
				</div>
				
				<div class="form-group">
					<div class="control-label col-xs-3">
						<form:label path="email">Email</form:label>
					</div>
					<div class="col-xs-6">
						<form:hidden path="id" value="${karatekaObject.id}" />
						<form:input cssClass="form-control" path="email"
							value="${karatekaObject.email}" />
					</div>
				</div>
				
				<div class="form-group">
					<form:label path="password" cssClass="control-label col-xs-3">Password</form:label>
					<div class="col-xs-6">
						<form:hidden path="id" value="${karatekaObject.id}" />
						<form:input cssClass="form-control" path="password"
							value="${karatekaObject.password}" />
					</div>
				</div>

				<div class="form-group">
					<div class="control-label col-xs-3">
						<form:label path="trainer">Is trainer?</form:label>
					</div>
					<div class="col-xs-6">
						<form:hidden path="id" value="${karatekaObject.trainer}" />
						<form:radiobutton path="trainer" value="true" /> Yes
						<form:radiobutton path="trainer" value="false" /> No
					</div>
				</div>
				
				<div class="form-group">
					<div class="control-label col-xs-3">
						<form:label path="clubId">Choose a Club :</form:label>
					</div>
					<div class="col-xs-6">
						<form:select path="clubId" required="true">
						<form:option value="">--Select--</form:option>	
						<form:options items="${clubs}" itemLabel="clubName" itemValue="id" />			
					</form:select>
					</div>
				</div>

				<div class="form-group">
					<div class="row">
						<div class="col-xs-4"></div>
						<div class="col-xs-4">
							<input type="submit" class="btn btn-primary" value="Update" />
						</div>
						<div class="col-xs-4"></div>
					</div>
				</div>
				<form:errors path="*" cssClass="errorblock" element="div" />
			</form:form>
		</div>
	</div>
</div>

<!-- Page content (End)    -->
<%@ include file="footer.jsp" %>